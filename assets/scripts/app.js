// Menggunakan metode array of objects
// const products = [
//     {
//         title: "A Pillow",
//         imageUrl: 'https://blog.jejualan.com/wp-content/uploads/2016/07/Product_Chair.jpg',
//         price: 20,
//         description: 'A Soft Pillow'
//     },
//     {
//         title: "A Carpet",
//         imageUrl: 'https://blog.jejualan.com/wp-content/uploads/2016/07/Product_Chair.jpg',
//         price: 40,
//         description: 'A Soft Carpet'
//     },
// ]

// Menggunakan metode objects
// const productList = {
//     products: [
//         {
//             title: "Logo ITS",
//             imageUrl: 'https://www.its.ac.id/wp-content/uploads/2020/07/Logo-ITS-1-355x219.png',
//             price: 20,
//             description: 'Ini adalah Logo ITS'
//         },
//         {
//             title: "Lambang ITS",
//             imageUrl: 'https://www.its.ac.id/wp-content/uploads/2020/07/Lambang-ITS-2-320x320.png',
//             price: 40,
//             description: 'Ini adalah Lambang ITS'
//         },
//     ],
//     render() {
//         const renderHook = document.getElementById('app') // Menggunakan DOM dengan id app
//         const prodList = document.createElement('ul')
//         prodList.className = 'product-list'
//         for (const prod of this.products) {
//             const prodEl = document.createElement('li')
//             prodEl.className = 'product-item'
//             prodEl.innerHTML = `
//                 <div>
//                     <img src="${prod.imageUrl}" alt="product-image">
//                     <div class="product_item_content">
//                         <h2>${prod.title}</h2>
//                         <h3>${prod.price}</h3>
//                         <p>${prod.description}</p>
//                         <button> Add to Cart </button>
//                     </div>
//                 </div>
//             `
//             prodList.append(prodEl)
//         }
//         renderHook.append(prodList)
//     }
// }
// productList.render()

// Menggunakan metode class & constructor
// class Product {
//     // title = 'Default'
//     // imageUrl
//     // description
//     // price

//     constructor(title, imageUrl, price, description) {
//         this.title = title
//         this.imageUrl = imageUrl
//         this.price = price
//         this.description = description
//     }
// }
// const productList = {
//     products: [
//         new Product('Logo ITS', 'https://www.its.ac.id/wp-content/uploads/2020/07/Logo-ITS-1-355x219.png', 20, 'Ini adalah Logo ITS'),
//         new Product('Lambang ITS', 'https://www.its.ac.id/wp-content/uploads/2020/07/Lambang-ITS-2-320x320.png', 40, 'Ini adalah Lambang ITS')
//     ],
//     render() {
//         const renderHook = document.getElementById('app') // Menggunakan DOM dengan id app
//         const prodList = document.createElement('ul')
//         prodList.className = 'product-list'
//         for (const prod of this.products) {
//             const prodEl = document.createElement('li')
//             prodEl.className = 'product-item'
//             prodEl.innerHTML = `
//                 <div>
//                     <img src="${prod.imageUrl}" alt="product-image">
//                     <div class="product_item_content">
//                         <h2>${prod.title}</h2>
//                         <h3>${prod.price}</h3>
//                         <p>${prod.description}</p>
//                         <button> Add to Cart </button>
//                     </div>
//                 </div>
//             `
//             prodList.append(prodEl)
//         }
//         renderHook.append(prodList)
//     }
// }
// productList.render()

// Menggunakan metode class Product List & constructor
// class Product {
//     constructor(title, imageUrl, price, description) {
//         this.title = title
//         this.imageUrl = imageUrl
//         this.price = price
//         this.description = description
//     }
// }
// class ProductItem {
//     constructor(product) {
//         this.product = product
//     }
//     processItem() {
//         const prodEl = document.createElement('li')
//         prodEl.className = 'product-list'
//         prodEl.innerHTML = `
//         <div>
//             <img src="${this.product.imageUrl}" alt="product-image">
//             <div class="product_item_content">
//                 <h2>${this.product.title}</h2>
//                 <h3>${this.product.price}</h3>
//                 <p>${this.product.description}</p>
//                 <button> Add to Cart </button>
//             </div>
//         </div>
//         `
//         return prodEl
//     }
// }
// class ProductList {
//     products = [
//         new Product('Logo ITS', 'https://www.its.ac.id/wp-content/uploads/2020/07/Logo-ITS-1-355x219.png', 20, 'Ini adalah Logo ITS'),
//         new Product('Lambang ITS', 'https://www.its.ac.id/wp-content/uploads/2020/07/Lambang-ITS-2-320x320.png', 40, 'Ini adalah Lambang ITS')
//     ]
//     constructor() {

//     }
//     render() {
//         const renderHook = document.getElementById('app') // Menggunakan DOM dengan id app
//         const prodList = document.createElement('ul')
//         prodList.className = 'product-list'
//         for (const prod of this.products) {
//             const productItem = new ProductItem(prod)
//             const prodEl = productItem.processItem() // Apend dibawah ini isinya li html tag (product item = satuan produk nya)
//             prodList.append(prodEl)
//         }
//         renderHook.append(prodList)
//     }
// }
// const productList = new ProductList()
// productList.render()

// Menggunakan DOM Eventlistener
class Product {
    // title= "Default"
    // imageUrl
    // description
    // price

    constructor(title, imageUrl, desc, price) {
        this.title = title
        this.imageUrl = imageUrl
        this.desc = desc
        this.price = price
    }
}

class Cart {
    items = []

    render() {
        const cartEl = document.createElement('section')
        cartEl.innerHTML = `
      <h1>Total: ${0}</h2>
      <button>Order Now!</button>
      `
        cartEl.className = 'cart'
        return cartEl
    }
}

class ProductItem {
    constructor(product) {
        this.product = product
    }


    addToCart() {
        console.log('nambah barang ke keranjang produk ini :')
        console.log(this.product)
    }

    processItem() {
        const prodEl = document.createElement('li')
        prodEl.className = 'product-item'
        prodEl.innerHTML = `
        <div>
          <img src="${this.product.imageUrl}" alt="product-image">
          <div class="product_item__content">
            <h2>${this.product.title}</h2>
            <h3>${this.product.price}</h3>
            <p>${this.product.description}</p> 
            <button>Add to Cart</button>
          </div>
        </div>
      `
        const addCartButton = prodEl.querySelector('button')
        addCartButton.addEventListener('click', this.addToCart.bind(this))
        return prodEl
    }
}

class ProductList {
    products = [
        new Product(
            'A Pillow',
            'https://blog.jejualan.com/wp-content/uploads/2016/07/Product_Chair.jpg',
            20,
            'A Soft Pillow'
        ),
        new Product(
            "A Carpet",
            'https://thumb.viva.co.id/media/frontend/thumbs3/2012/06/01/157172_karpet_1265_711.JPG',
            200,
            'A Carpet can fly'
        )
    ]

    constructor() { }

    render() {
        // dibawah ini intinya bikin html code yang di append(masukkan) ke dalam html div yg id nya app
        const prodList = document.createElement('ul')
        prodList.className = 'product-list'
        for (const prod of this.products) {
            const productItem = new ProductItem(prod)
            const prodEl = productItem.processItem()
            // append dibawah ini isinya li html tag (product item = satuan produk nya)
            prodList.append(prodEl)
        }
        return prodList
    }
}

class Shop {
    render() {
        const renderHook = document.getElementById('app')

        // component cart nya
        const cart = new Cart()
        const cartEl = cart.render()

        // manggil component product list
        const productList = new ProductList()
        const prodListEl = productList.render()

        // render append ke div id app
        renderHook.append(cartEl)
        renderHook.append(prodListEl)
    }
}

const shop = new Shop()
shop.render()
